package com.plan.action;

import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.plan.bean.TrainPlanInfo;
import com.plan.bean.TrainPlanItem;
import com.plan.server.AddServer;
import com.plan.util.uuid;

public class AddAction extends ActionSupport{
	TrainPlanInfo trainPlanInfo = new TrainPlanInfo();
	List<TrainPlanItem> trainPlanItems = new ArrayList<TrainPlanItem>();
	private static AddServer addServer = new AddServer();
	
	public TrainPlanInfo getTrainPlanInfo() {
		return trainPlanInfo;
	}

	public List<TrainPlanItem> getTrainPlanItems() {
		return trainPlanItems;
	}

	public void setTrainPlanItems(List<TrainPlanItem> trainPlanItems) {
		this.trainPlanItems = trainPlanItems;
	}

	public void setTrainPlanInfo(TrainPlanInfo trainPlanInfo) {
		this.trainPlanInfo = trainPlanInfo;
	}

	public String doSave() throws Exception {
		String plan_Id = uuid.getUUid();
		trainPlanInfo.setTRAIN_PLAN_ID(plan_Id);
		if(addServer.saveInfo(trainPlanInfo)){
			for (TrainPlanItem t : trainPlanItems) {
				t.settRAIN_PLAN_ID(plan_Id);
				t.setTRAIN_ITEM_ID(uuid.getUUid());
				if (addServer.saveItem(t)) {
					
				} else {
					return ERROR;
				}
			}
		} else {
			return ERROR;
		}
		return SUCCESS;
	}
}
