package com.plan.action;

import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.plan.bean.TrainPlan;
import com.plan.bean.TrainPlanInfo;
import com.plan.bean.TrainPlanItem;
import com.plan.server.SearchServer;

public class SearchAction extends ActionSupport{
	TrainPlan trainPlan = new TrainPlan();
	List<TrainPlan> trainPlans = new ArrayList<TrainPlan>();
	

	public List<TrainPlan> getTrainPlans() {
		return trainPlans;
	}


	public void setTrainPlans(List<TrainPlan> trainPlans) {
		this.trainPlans = trainPlans;
	}


	public TrainPlan getTrainPlan() {
		return trainPlan;
	}


	public void setTrainPlan(TrainPlan trainPlan) {
		this.trainPlan = trainPlan;
	}


	public String doSearch(){
		trainPlans = SearchServer.searchAll(trainPlan);
		return SUCCESS;
	}
}
